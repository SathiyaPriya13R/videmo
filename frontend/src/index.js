
import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import App from "App";
import store from './store';
import { Provider } from 'react-redux';


// Soft UI Context Provider
import { SoftUIControllerProvider } from "context";
import { ProfileUIControllerProvider } from "context";
import {AgencyUIControllerProvider } from "context";


const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <BrowserRouter>
  <AgencyUIControllerProvider>
    <ProfileUIControllerProvider>
    <SoftUIControllerProvider>
    <Provider store={store}>

      <App />
      </Provider>,

    </SoftUIControllerProvider>
    </ProfileUIControllerProvider>
    </AgencyUIControllerProvider>
  </BrowserRouter>
);
