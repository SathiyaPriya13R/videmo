
import React, { useState } from "react";
import PropTypes from "prop-types";
import Card from "@mui/material/Card";
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Grid from "@mui/material/Grid";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Box from "@mui/material/Box";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import SoftSelect from "components/SoftSelect";
import SoftButton from "components/SoftButton";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
// import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";
import selectData from "layouts/pages/account/settings/components/BasicInfo/data/selectData";
import SoftInputDateTime from "layouts/ecommerce/products/products-list/activeJobs/SoftInputDateTime";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
// import Questions from "examples/Lists/Questions";
import FormField from "layouts/pages/account/components/FormField";
import {Table,TableBody, TableCell,TableContainer,TableRow,Paper,} from "@mui/material";
import { useLocation } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';

const optionInterviewTime = [
  { label: "15 min", value: "15 min" }, 
  { label: "30 min", value: "30 min" },
  { label: "60 min", value: "60 min" },
];
const options = [
  { label: "Option 1", value: "Option 1" }, 
  { label: "Option 2", value: "Option 2" },
  { label: "Option 3", value: "Option 3" },
];

function NewSchedule() {
  const location = useLocation();
  const { arrayValue } = location.state || {};
  // const [skills, setSkills] = useState(["react", "angular"]);
  const [startDate, setStartDate] = useState(""); // State for Start Date
  const [linkDate, setLinkDate] = useState(""); // State for Start Date
  const [tabValue, setTabValue] = useState(0);
  // const [openModal, setOpenModal] = useState(false);
  // const [fileType, setFileType] = useState(""); // State for file type
  const [openGroupDialog, setOpenGroupDialog] = useState(false); // State for group dialog
  // const [selectedGroup, setSelectedGroup] = useState(""); // State for selected group
  const [category, setCategory] = useState("IT");
  const[InterviewTime,setIntervieTime]=useState("");
  const [savedGroupName, setSavedGroupName] = useState(""); // State for saved group name
  const [groupName, setGroupName] = useState(""); // State for group name input
  const [sector, setSector] = useState("");
  const [position, setPosition] = useState("");
  const [primarySkills, setPrimarySkills] = useState("");
  const [secondarySkills, setSecondarySkills] = useState("");
  const [tertiarySkills, setTertiarySkills] = useState("");
  const [level, setLevel] = useState("");
  // const [questions, setQuestions] = useState(Array(10).fill(false));
  const [customQuestion, setCustomQuestion] = useState("");
  const [customAnswer, setCustomAnswer] = useState("");
  const [savedQuestions, setSavedQuestions] = useState([]);
  const [intquestions, setIntQuestions] = useState([]);


  const handleTabChange = (event, newValue) => {
    setTabValue(newValue);
  };
  

  const handleCustomQuestionChange = (event) => {
    setCustomQuestion(event.target.value);
  };

  const handleCustomAnswerChange = (event) => {
    setCustomAnswer(event.target.value);
  };

  const handleAddQuestion = () => {
    setSavedQuestions([...savedQuestions, { question: customQuestion, answer: customAnswer }]);
    setCustomQuestion("");
    setCustomAnswer("");
  };

  const handleOpenGroupDialog = () => {
    setOpenGroupDialog(true);
  };

  const handleCloseGroupDialog = () => {
    setOpenGroupDialog(false);
  };

  const handleSaveGroup = () => {
    setSavedGroupName(groupName); // Update saved group name
    setOpenGroupDialog(false);
  };

  const handleGroupNameChange = (event) => {
    setGroupName(event.target.value);
  };

  const handleCategoryChange = (event) => {
    setCategory(event.target.value);
  };

  const handleSectorChange = (event) => {
    setSector(event.value);
  };

  const handlePositionChange = (event) => {
    setPosition(event.value);
  };

  const handlePrimarySkillsChange = (event) => {
    setPrimarySkills(event.value);
    axios.get(`http://127.0.0.1:5000/api/schedule/get_questions/${event.value}`)
    .then(response => {
      console.log('Fetched questions:', response.data); // Log data to the console
      setIntQuestions(response.data);
    })
    .catch(error => {
      console.error('Error fetching questions:', error); // Log error to the console
      setError(error);
    });
  };

  const handleSecondarySkillsChange = (event) => {
    setSecondarySkills(event.value);
  };

  const handleTertiarySkillsChange = (event) => {
    setTertiarySkills(event.value);
  };

  const handleLevelChange = (event) => {
    setLevel(event.value);
  };
  const handleInterviewTime = (event) =>{
    setIntervieTime(event.value);

  };



  const Basicquestion = intquestions.filter(item=> item.Level==="Basic")
  const intermidiatequestion = intquestions.filter(item=> item.Level==="Intermediate")
  const advancedquestion = intquestions.filter(item=> item.Level==="Advanced")
  
  
  const [IntermidiatecheckedQuestions, setIntermidiateCheckedQuestions] = useState( Array(intermidiatequestion.length).fill(false) );
  const IntermidiatehandleQuestionChange = (index) => {
    const newCheckedQuestions = [...IntermidiatecheckedQuestions];
    newCheckedQuestions[index] = !newCheckedQuestions[index];
    setIntermidiateCheckedQuestions(newCheckedQuestions);
  };
  const [basiccheckedQuestions, setbasicCheckedQuestions] = useState( Array(Basicquestion.length).fill(false) );
  const basichandleQuestionChange = (index) => {
    const newCheckedQuestions = [...basiccheckedQuestions];
    newCheckedQuestions[index] = !newCheckedQuestions[index];
    setbasicCheckedQuestions(newCheckedQuestions);
  };
  const [AdvancedcheckedQuestions, setAdvancedCheckedQuestions] = useState( Array(advancedquestion.length).fill(false) );
  const AdvancedhandleQuestionChange = (index) => {
    const newCheckedQuestions = [...AdvancedcheckedQuestions];
    newCheckedQuestions[index] = !newCheckedQuestions[index];
    setAdvancedCheckedQuestions(newCheckedQuestions);
  };
  const today = new Date(); // Get the current date

  // Format the date using toLocaleDateString with options
  const formattedDate = today.toLocaleDateString('en-US', {
    day: '2-digit',
    month: 'long',
    year: 'numeric',
  });
  
  // Format the time using toLocaleTimeString with options
  const formattedTime = today.toLocaleTimeString('en-US', {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
    hour12: true, // for 12-hour format with AM/PM
  });
  const formattedDateTime = `${formattedDate} ${formattedTime}`;
  const handleSave = async () => {
    if(arrayValue.length>0)
      {

    const AdvancedcheckedValues = AdvancedcheckedQuestions
    .map((checked, index) => (checked ? advancedquestion[index] : null))
    .filter(question => question !== null);
  

  const basiccheckedValues = basiccheckedQuestions
  .map((checked, index) => (checked ? Basicquestion[index] : null))
  .filter(question => question !== null);


  const intermidiatecheckedValues = IntermidiatecheckedQuestions
    .map((checked, index) => (checked ? intermidiatequestion[index] : null))
    .filter(question => question !== null);

 
    
  let objectArray = [...basiccheckedValues,...intermidiatecheckedValues,...AdvancedcheckedValues];



    for (const item of arrayValue) {
    const scheduleData = {
      category,
      sector,
      position,
      primarySkills,
      secondarySkills,
      tertiarySkills,
      level,
      InterviewTime,
      startDate,
      linkDate,
      "CreatedDate":formattedDateTime,
      "interviewquestions":objectArray,
      "groupname":savedGroupName,
      "Candidate_id":item.candidate_id,
      "candidate_email":item.email,
      "meet_id":`http://localhost:3001/${item.candidate_id}`

     
    };
    try {
      const response = await fetch("http://localhost:5000/api/schedule/save", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(scheduleData),
      });

      if (response.ok) {
        console.log("Schedule data saved successfully");
        Swal.fire({
          title: 'Success!',
          text: 'Data saved successfully',
          icon: 'success',
          confirmButtonText: 'OK'
        });
      } else {
        console.error("Failed to save schedule data");
       
      }
    } catch (error) {
      console.error("Error saving schedule data:", error);
    }
  }
  }
  };
 
  const [error, setError] = useState(null);


  if (error) {
    return <div>Error: {error.message}</div>;
  }
  return (
    <DashboardLayout>
      <DashboardNavbar />
      <SoftBox py={3}>
        <SoftBox mb={3}>
          <Card id="basic-infoo" sx={{ overflow: "visible" }}>
            <SoftTypography variant="h6" mb={2} sx={{ marginLeft: "20px", marginTop: "30px" }}>
              New Schedule
            </SoftTypography>
            <SoftBox component="form" pb={3} px={3}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <SoftBox
                    display="flex"
                    flexDirection="column"
                    justifyContent="flex-end"
                    height="100%"
                  >
                    <RadioGroup row value={category} onChange={handleCategoryChange}>
                      <FormControlLabel value="IT" control={<Radio />} label="IT" />
                      <FormControlLabel value="Non-IT" control={<Radio />} label="Non-IT" />
                    </RadioGroup>
                  </SoftBox>
                </Grid>
                <Grid item xs={12}>
                  <Grid container spacing={3}>
                    <Grid item xs={12}>
                      <SoftBox
                        display="flex"
                        flexDirection="column"
                        justifyContent="flex-end"
                        height="100%"
                      >
                        <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                          <SoftTypography
                            component="label"
                            variant="caption"
                            fontWeight="bold"
                            textTransform="capitalize"
                          >
                            Sector
                          </SoftTypography>
                        </SoftBox>
                        <SoftSelect
                          placeholder="Enter your Sector"
                          options={selectData.sector}
                          onChange={handleSectorChange}
                        />
                      </SoftBox>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <SoftBox
                    display="flex"
                    flexDirection="column"
                    justifyContent="flex-end"
                    height="100%"
                  >
                    <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                      <SoftTypography
                        component="label"
                        variant="caption"
                        fontWeight="bold"
                        textTransform="capitalize"
                      >
                        Position Applied For
                      </SoftTypography>
                    </SoftBox>
                    <SoftSelect
                      placeholder="Position Applied For"
                      options={selectData.position}
                      onChange={handlePositionChange}
                    />
                  </SoftBox>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <SoftBox
                    display="flex"
                    flexDirection="column"
                    justifyContent="flex-end"
                    height="100%"
                  >
                    <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                      <SoftTypography
                        component="label"
                        variant="caption"
                        fontWeight="bold"
                        textTransform="capitalize"
                      >
                        Primary Skills
                      </SoftTypography>
                    </SoftBox>
                    <SoftSelect
                      placeholder="Enter your Primary Skills"
                      options={selectData.primaryskills}
                      onChange={handlePrimarySkillsChange}
                    />
                  </SoftBox>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <SoftBox
                    display="flex"
                    flexDirection="column"
                    justifyContent="flex-end"
                    height="100%"
                  >
                    <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                      <SoftTypography
                        component="label"
                        variant="caption"
                        fontWeight="bold"
                        textTransform="capitalize"
                      >
                        Secondary Skills
                      </SoftTypography>
                    </SoftBox>
                    <SoftSelect
                      placeholder="Secondary Skills"
                      options={selectData.secondaryskills}
                      onChange={handleSecondarySkillsChange}
                    />
                  </SoftBox>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <SoftBox
                    display="flex"
                    flexDirection="column"
                    justifyContent="flex-end"
                    height="100%"
                  >
                    <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                      <SoftTypography
                        component="label"
                        variant="caption"
                        fontWeight="bold"
                        textTransform="capitalize"
                      >
                        Tertiary Skills
                      </SoftTypography>
                    </SoftBox>
                    <SoftSelect
                      placeholder="Enter your Tertiary Skills"
                      options={selectData.tertiaryskills}
                      onChange={handleTertiarySkillsChange}
                    />
                  </SoftBox>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <SoftBox
                    display="flex"
                    flexDirection="column"
                    justifyContent="flex-end"
                    height="100%"
                  >
                    <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                      <SoftTypography
                        component="label"
                        variant="caption"
                        fontWeight="bold"
                        textTransform="capitalize"
                      >
                        Level
                      </SoftTypography>
                    </SoftBox>
                    <SoftSelect
                      placeholder="Select Level"
                      options={options}
                      onChange={handleLevelChange}
                    />
                  </SoftBox>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <SoftBox
                    display="flex"
                    flexDirection="column"
                    justifyContent="flex-end"
                    height="100%"
                  >
                    <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                      <SoftTypography
                        component="label"
                        variant="caption"
                        fontWeight="bold"
                        textTransform="capitalize"
                      >
                        Interview Time (Mins)
                      </SoftTypography>
                    </SoftBox>
                    <SoftSelect placeholder="Eg: 60" options={optionInterviewTime}
                      onChange={handleInterviewTime}/>
                  </SoftBox>
                </Grid>
 
                <Grid item xs={12} sm={6}>
                  <SoftInputDateTime
                    label="Start Date"
                    value={startDate}
                    onChange={(e) => setStartDate(e.target.value)}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <SoftInputDateTime
                    label="Interview Link Expiry Date"
                    value={linkDate}
                    onChange={(e) => setLinkDate(e.target.value)}
                  />
                </Grid>


              </Grid>
              </SoftBox>
            <Accordion>
              <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                <SoftTypography variant="h6">Questions</SoftTypography>
              </AccordionSummary>
              <AccordionDetails>
                <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                  <Tabs value={tabValue} onChange={handleTabChange}>
                    <Tab label="Populate Questions" />
                    <Tab label="Custom Questions" />
                  </Tabs>
                </Box>
                <TabPanel value={tabValue} index={0}>
                  <SoftBox mt={5} mb={3}>
                    <Grid container spacing={3}>
                      <Grid item xs={12} md={6} xl={4}>
                      {/* <PlatformNew /> */}
                      <Card>
                          <SoftBox pt={2} px={2}>
                            <SoftBox mt={3}>
                              <SoftTypography
                                variant="caption"
                                fontWeight="bold"
                                color="text"
                                textTransform="uppercase"
                              >
                                basic
                              </SoftTypography>
                            </SoftBox>
                            <SoftBox
                              style={{ maxHeight: "420px", overflowY: "auto" }} // Set a max height and enable scroll
                            >
                              {intquestions.map((PlatformNew, index) => (
                                <SoftBox display="flex" py={1} mb={0.25} key={index}>
                                  <SoftBox mt={0.25}>
                                  <Checkbox
            checked={basiccheckedQuestions[index]}
            onChange={() => basichandleQuestionChange(index)}
          />
                                  </SoftBox>
                                  <SoftBox width="80%" ml={2}>
                                    <SoftTypography
                                      variant="button"
                                      fontWeight="regular"
                                      color="text"
                                    >
                                      {PlatformNew.Questions}
                                    </SoftTypography>
                                  </SoftBox>
                                </SoftBox>
                              ))}
                            </SoftBox>
                          </SoftBox>
                        </Card>
                      </Grid>
                      <Grid item xs={12} md={6} xl={4}>
                        <Card>
                          <SoftBox pt={2} px={2}>
                            <SoftBox mt={3}>
                              <SoftTypography
                                variant="caption"
                                fontWeight="bold"
                                color="text"
                                textTransform="uppercase"
                              >
                                Intermediate
                              </SoftTypography>
                            </SoftBox>
                            <SoftBox
                              style={{ maxHeight: "420px", overflowY: "auto" }} // Set a max height and enable scroll
                            >
                              {intermidiatequestion.map((questionText, index) => (
                                <SoftBox display="flex" py={1} mb={0.25} key={index}>
                                  <SoftBox mt={0.25}>
                                  <Checkbox
            checked={IntermidiatecheckedQuestions[index]}
            onChange={() => IntermidiatehandleQuestionChange(index)}
          />
                                  </SoftBox>
                                  <SoftBox width="80%" ml={2}>
                                    <SoftTypography
                                      variant="button"
                                      fontWeight="regular"
                                      color="text"
                                    >
                                      {questionText.Questions}
                                    </SoftTypography>
                                  </SoftBox>
                                </SoftBox>
                              ))}
                            </SoftBox>
                          </SoftBox>
                        </Card>
                      </Grid>
                      <Grid item xs={12} xl={4}>
                        {/* <Questions /> */}
                        <Card>
                          <SoftBox pt={2} px={2}>
                            <SoftBox mt={3}>
                              <SoftTypography
                                variant="caption"
                                fontWeight="bold"
                                color="text"
                                textTransform="uppercase"
                              >
                                Advanced
                              </SoftTypography>
                            </SoftBox>
                            <SoftBox
                              style={{ maxHeight: "420px", overflowY: "auto" }} // Set a max height and enable scroll
                            >
                              {advancedquestion.map((question, index) => (
                                <SoftBox display="flex" py={1} mb={0.25} key={index}>
                                  <SoftBox mt={0.25}>
                                  <Checkbox
            checked={AdvancedcheckedQuestions[index]}
            onChange={() => AdvancedhandleQuestionChange(index)}
          />
                                  </SoftBox>
                                  <SoftBox width="80%" ml={2}>
                                    <SoftTypography
                                      variant="button"
                                      fontWeight="regular"
                                      color="text"
                                    >
                                      {question.Questions}
                                    </SoftTypography>
                                  </SoftBox>
                                </SoftBox>
                              ))}
                            </SoftBox>
                          </SoftBox>
                        </Card>
                      </Grid>
                    </Grid>
                  </SoftBox>
                </TabPanel>
                <TabPanel value={tabValue} index={1}>
                  <SoftBox component="form" pb={3} px={3}>
                    <Grid container spacing={3}>
                      <Grid item xs={12}>
                        <Grid container spacing={3}>
                          <Grid item xs={12}>
                            <FormField
                              label="questions"
                              placeholder="Questions"
                              value={customQuestion}
                              onChange={handleCustomQuestionChange}
                            />
                          </Grid>
                          <Grid item xs={12}>
                            <FormField
                              label="answer"
                              placeholder="Answer"
                              value={customAnswer}
                              onChange={handleCustomAnswerChange}
                            />
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  </SoftBox>
                  <SoftBox ml={{ xs: 0, sm: 5 }} mt={{ xs: 2, sm: 2 }} mb={{ xs: 2, sm: 5 }}>
                    <SoftButton
                      variant="gradient"
                      color="info"
                      size="small"
                      onClick={handleAddQuestion}
                    >
                      Add Question
                    </SoftButton>
                  </SoftBox>
                  <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
                      <TableBody>
                        <TableRow>
                          <TableCell>Question</TableCell>
                          <TableCell>Answer</TableCell>
                        </TableRow>
                        {savedQuestions.map((item, index) => (
                          <TableRow
                            key={index}
                            sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                          >
                            <TableCell>{item.question}</TableCell>
                            <TableCell>{item.answer}</TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </TableContainer>
                </TabPanel>
              </AccordionDetails>

              <SoftBox ml={{ xs: 0, sm: 5 }} mt={{ xs: 2, sm: 2 }} mb={{ xs: 2, sm: 5 }}>
                <SoftButton
                  variant="gradient"
                  color="info"
                  size="small"
                  onClick={handleOpenGroupDialog}
                >
                  Add group
                </SoftButton>
               
              </SoftBox>

              {savedGroupName && (
                <SoftBox ml={{ xs: 0, sm: 5 }} mt={{ xs: 2, sm: 2 }} mb={{ xs: 2, sm: 5 }}>
                  <SoftTypography>{`Group Name: ${savedGroupName}`}</SoftTypography>
                </SoftBox>
              )}
              <SoftBox ml={{ xs: 0, sm: 5 }} mt={{ xs: 2, sm: 2 }} mb={{ xs: 2, sm: 5 }}>
               <SoftButton variant="gradient" color="info" size="small" onClick={handleSave}>
                  Save
                 </SoftButton>
               </SoftBox>

            </Accordion>
          </Card>
        </SoftBox>
      </SoftBox>
      {/* Add Group Dialog */}
      <Dialog open={openGroupDialog} onClose={handleCloseGroupDialog}>
        <DialogTitle>Enter Your Group</DialogTitle>
        <DialogContent>
          <Grid item xs={12}>
            <FormField
              label="Group name"
              placeholder="Eg: React"
              value={groupName}
              onChange={handleGroupNameChange}
            />
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseGroupDialog} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSaveGroup} color="primary">
            Save
          </Button>
        </DialogActions>
      </Dialog>
      {/* Display saved group name */}

      {/* Button to open group dialog */}
      {/* <SoftBox ml={{ xs: 0, sm: 5 }} mt={{ xs: 2, sm: 2 }} mb={{ xs: 2, sm: 5 }}>
        <Button variant="outlined" color="primary" onClick={handleOpenGroupDialog}>
          Add Group
        </Button>
      </SoftBox>{" "} */}
    </DashboardLayout>
  );
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`tabpanel-${index}`}
      aria-labelledby={`tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <SoftTypography>{children}</SoftTypography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  value: PropTypes.any.isRequired,
  index: PropTypes.any.isRequired,
};

export default NewSchedule;
