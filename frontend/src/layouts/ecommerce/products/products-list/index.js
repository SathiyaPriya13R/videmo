/* eslint-disable react/prop-types */
import React, { useState,useEffect } from "react";
import axios from 'axios';
// react-router-dom components
import { Link } from "react-router-dom";
 
// @mui material components
import Card from "@mui/material/Card";
import Stack from "@mui/material/Stack";
import { Icon, Tooltip, IconButton, Menu, MenuItem, Dialog, DialogTitle, DialogContent, Autocomplete, TextField, DialogActions, Button } from "@mui/material";
import { Description, PlayCircleFilled } from "@mui/icons-material";
// Soft UI Dashboard PRO components
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import SoftButton from "components/SoftButton";
import SoftBadge from "components/SoftBadge";
import Checkbox from "@mui/material/Checkbox";
// Soft UI Dashboard PRO example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";
import DataTable from "examples/Tables/DataTable";
 
// Data
//import dataTableData from "layouts/ecommerce/products/products-list/data/dataTableData";
import { FilterList } from "@mui/icons-material";
import SoftInputDateTime from "./activeJobs/SoftInputDateTime";
import DownloadIcon from "../products-list/components/ActionCell/DownloadIcon";
import EventAvailableIcon from "../products-list/components/ActionCell/EventAvailableIcon";
 
function ProductsList() {
  const [anchorEl, setAnchorEl] = useState(null);
  // const [selectedEmails, setSelectedEmails] = useState({ emails: [], candidateids: [] });
  const [selectedEmails, setSelectedEmails] = useState([]);
  const [candinfo, setCandinfo] = useState([]);
  const [error, setError] = useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
 
  const handleClose = () => {
    setAnchorEl(null);
  };
 
 
  const [open, setOpen] = useState(false);
  const [field1, setField1] = useState('');
  const [field2, setField2] = useState('');
  const [field3, setField3] = useState('');
 
  const handleClickOpen = () => {
    setOpen(true);
  };
 
  const handleClose1 = () => {
    setOpen(false);
  };
  const [startDate, setStartDate] = useState(null);
 
 
  const top100Films = [
    { label: 'ABC234SD2', year: 1994 },
    { label: 'ABC234SD22', year: 1972 },
    { label: 'ABC234SD23', year: 1974 },
  ];
  const jobtitle = [
    { label: 'Web developer', year: 1994 },
    { label: 'Soft ware developer', year: 1972 },
    { label: 'App developer', year: 1974 },
  ];
  const statusDropdown = [
    { label: 'Active', year: 1994 },
    { label: 'In Active', year: 1972 },
    { label: 'completed', year: 1974 },
  ];
  const handleApply = () => {
    // Handle the apply logic here
    console.log('Field 1:', field1);
    console.log('Field 2:', field2);
    console.log('Field 3:', field3);
    setOpen(false);
  };
 
   const outOfStock = (
    <SoftBadge variant="contained" color="error" size="xs" badgeContent="inactive" container />
  );
  const inStock = (
    <SoftBadge variant="contained" color="success" size="xs" badgeContent="active" container />
  );
 
  useEffect(() => {
    // Fetch schedules from the API using axios
    axios.get('http://127.0.0.1:5000/api/candidate/get_candidate')
      .then(response => {
        console.log('Fetched schedules:', response.data);  // Log data to the console
        setCandinfo(response.data);
      })
      .catch(error => {
        console.error('Error fetching schedules:', error);  // Log error to the console
        setError(error);
      });
  }, []);
 
  if (error) {
    return <div>Error: {error.message}</div>;
  }
  const result = candinfo.map(({ firstname,email,contact_number  }) => ({ firstname,email,contact_number,resume: <Description sx={{width:"1.5em" , height:"1.5em"}} />,video:<DownloadIcon />,grade:"5",status: "active",action: <EventAvailableIcon passedvalue={selectedEmails}/>,}));
  console.log(result);
 
 
  const handleCheckboxChange = (email, candidate_id, checked) => {
    setSelectedEmails(prevEmails => {
      if (checked) {
        // Add the email and candidateid to the selected emails array if not already included
        const newEmails = [...prevEmails, { email, candidate_id }];
        console.log("Selected Emails:", newEmails);  // Log the updated array of selected emails
        return newEmails;
      } else {
        // Remove the email and candidateid from the selected emails array if unchecked
        const newEmails = prevEmails.filter(item => item.email !== email || item.candidate_id !== candidate_id);
        console.log("Selected Emails:", newEmails);  // Log the updated array of selected emails
        return newEmails;
      }
    });
  };
  const dataTableData=  {
    columns: [
      {
        Header: "",
        accessor: "checkbox",
        width: "5%",
        Cell: ({ row }) => (
          <Checkbox
            checked={selectedEmails.some(item => item.email === row.original.email && item.candidate_id === row.original.candidate_id)}
            onChange={(e) => handleCheckboxChange(row.original.email, row.original.candidate_id, e.target.checked)}
          />
        ),
      },
      // {
      //   Header: "Candidate ID",
      //   accessor: "candidate_id",
      //   width: "17%",
      // },
 
      {
        Header: "Candidate Name",
        accessor: "firstname",
        width: "17%",
      },
      { Header: "Email", accessor: "email",width: "15%", },
      { Header: "mobile no", accessor: "contact_number" , width:"15%" },
      { Header: "Resume", accessor: "resume", width:"15%"  },
      { Header: "video", accessor: "video" , width:"15%" },
      { Header: "grade", accessor: "grade" , width:"15%" },
      {
        Header: "status",
        accessor: "status",
        Cell: ({ value }) => (value === "active" ? inStock : outOfStock),
      },
      { Header: "action", accessor: "action" },
    ],
   
    rows: result
  };
  return (
    <DashboardLayout>
      <DashboardNavbar />
      <SoftBox my={3}>
        <Card>
          <SoftBox display="flex" justifyContent="space-between" alignItems="flex-start" p={3}>
            <SoftBox display="flex" alignItems="center" lineHeight={1}>
              <SoftTypography variant="h5" fontWeight="medium">
                All Candidates
              </SoftTypography>
              <SoftBox ml={1}>
                <Tooltip>
                  <IconButton onClick={handleClick}>
                    <Icon>arrow_drop_down</Icon>
                  </IconButton>
                </Tooltip>
                <Menu
                  anchorEl={anchorEl}
                  open={Boolean(anchorEl)}
                  onClose={handleClose}
                >
 
                  <MenuItem onClick={handleClose}>Active</MenuItem>
                  <MenuItem onClick={handleClose}>Inactive</MenuItem>
                  <MenuItem onClick={handleClose}>Completed</MenuItem>
                </Menu>
              </SoftBox>
            </SoftBox>
            <Stack spacing={1} direction="row">
              {/* <SoftButton variant="outlined" color="info" size="small" startIcon={<FilterList />} sx={{ height: "46px" }}>
                Filter
              </SoftButton> */}
               <div>
   <SoftButton variant="gradient" color="info" size="small" sx={{marginRight: 1 , height:"46px"}} startIcon={<FilterList />} onClick={handleClickOpen}>
                Filter
              </SoftButton>
              <Dialog open={open} onClose={handleClose1} maxWidth="md">
        {/* <DialogTitle>Filter</DialogTitle> */}
        <DialogContent sx={{ width: 500 }}>
          <div style={{ display: 'flex', flexDirection: 'column', gap: '20px',padding:"46px" }}>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <SoftTypography style={{ marginRight: '20px', minWidth: '120px' , fontSize:"1rem" }}>Date</SoftTypography>
              <SoftInputDateTime
                  // label="Start Date"
                  value={startDate}
                  onChange={(e) => setStartDate(e.target.value)}
                  sx={{width:"500px !important" }}
                />
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <SoftTypography style={{ marginRight: '20px', minWidth: '120px', fontSize:"1rem"  }}>Job ID</SoftTypography>
              <Autocomplete
                disablePortal
                id="name-autocomplete"
                options={top100Films}
                sx={{ width: '100%' }}
                renderInput={(params) => <TextField {...params} placeholder="Job ID"/>}
              />
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <SoftTypography style={{ marginRight: '20px', minWidth: '120px', fontSize:"1rem"  }}>Job Title</SoftTypography>
              <Autocomplete
                disablePortal
                id="age-autocomplete"
                options={jobtitle}
                sx={{ width: '100%' }}
                renderInput={(params) => <TextField {...params} placeholder="Job Title"/>}
              />
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <SoftTypography style={{ marginRight: '20px', minWidth: '120px', fontSize:"1rem"  }}>Status</SoftTypography>
              <Autocomplete
                disablePortal
                id="location-autocomplete"
                options={statusDropdown}
                sx={{ width: '100%' }}
                renderInput={(params) => <TextField {...params} placeholder="Status" />}
              />
            </div>
          </div>
        </DialogContent>
        <DialogActions>
          <SoftButton variant="gradient" color="info" onClick={handleClose1} >
            Reset
          </SoftButton>
          <SoftButton variant="gradient" color="info" onClick={handleApply} >
            Apply
          </SoftButton>
        </DialogActions>
      </Dialog>
 
    </div>
              <Link to="/newCandidate">
                <SoftButton variant="gradient" color="info" size="small" sx={{ height: "46px" }}>
                  + Add Resume
                </SoftButton>
              </Link>
            </Stack>
          </SoftBox>
          <DataTable
            table={dataTableData}
            entriesPerPage={{
              defaultValue: 7,
              entries: [5, 7, 10, 15, 20, 25],
            }}
            canSearch
          />
        </Card>
      </SoftBox>
      <Footer />
    </DashboardLayout>
  );
}
 
export default ProductsList;