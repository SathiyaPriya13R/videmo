import React, { useEffect, useState } from "react";
// react-router-dom components


// @mui material components
import Card from "@mui/material/Card";
import Stack from "@mui/material/Stack";
import {
  Icon,
  Tooltip,
  IconButton,
  Menu,
  MenuItem,
  Dialog,

  DialogContent,
  Autocomplete,
  TextField,
  InputAdornment,
  DialogActions,

} from "@mui/material";
import { FilterList, Search } from "@mui/icons-material";

// Soft UI Dashboard PRO components
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import SoftButton from "components/SoftButton";

// Soft UI Dashboard PRO example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";
import DataTable from "examples/Tables/DataTable";

// Data
// import dataTableData from "layouts/ecommerce/products/products-list/data/dataTableData";
// import dataTableData from "../data/dataTableData";

import SoftInputDateTime from "../activeJobs/SoftInputDateTime";
import dataHireFreshers from "./dataHireFreshers";

function HireFreshers() {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const [open, setOpen] = useState(false);
  const [field1, setField1] = useState("");
  const [field2, setField2] = useState("");
  const [field3, setField3] = useState("");

  useEffect(() => {
    // Your side effect logic here
    console.log('One of the fields was updated');
  }, [setField1, setField2, setField3]);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose1 = () => {
    setOpen(false);
  };
  const [startDate, setStartDate] = useState(null);

  const top100Films = [
    { label: "ABC234SD2", year: 1994 },
    { label: "ABC234SD22", year: 1972 },
    { label: "ABC234SD23", year: 1974 },
  ];


  const clientdrop = [
    { label: "Bala", year: 1994 },
    { label: "Vadivel", year: 1972 },
    { label: "Prabha", year: 1974 },
  ];
  const handleApply = () => {
    // Handle the apply logic here
    console.log("Field 1:", field1);
    console.log("Field 2:", field2);
    console.log("Field 3:", field3);
    setOpen(false);
  };
  const statusdropdown = [
    { label: "Active", year: 1994 },
    { label: "In Active", year: 1972 },
    { label: "Completed", year: 1974 },
  ];

  return (
    <DashboardLayout>
      <DashboardNavbar />
      <SoftBox my={3}>
        <Card>
          <SoftBox display="flex" justifyContent="space-between" alignItems="flex-start" p={3}>
            <SoftBox display="flex" alignItems="center" lineHeight={1}>
              <SoftTypography variant="h5" fontWeight="medium">
                Hire Freshers
              </SoftTypography>
              <SoftBox ml={1}>
                <Tooltip>
                  <IconButton onClick={handleClick}>
                    <Icon>arrow_drop_down</Icon>
                  </IconButton>
                </Tooltip>
                <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleClose}>
                  <MenuItem onClick={handleClose}>Active</MenuItem>
                  <MenuItem onClick={handleClose}>Inactive</MenuItem>
                  <MenuItem onClick={handleClose}>Completed</MenuItem>
                </Menu>
              </SoftBox>
            </SoftBox>
            <Stack spacing={1} direction="row">
              <TextField
                variant="outlined"
                size="small"
                placeholder="Search"
                //   value={search}
                //   onChange={(e) => setSearch(e.target.value)}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <IconButton>
                        <Search />
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
                sx={{
                  ".css-15v2jta-MuiInputBase-root-MuiOutlinedInput-root": {
                    height: "46px !important",
                  },
                  marginRight: 1,
                }}
              />
              {/* <SoftButton variant="outlined" color="info" size="small"  startIcon={<FilterList />}>
                Filter
              </SoftButton> */}
              <div>
                <SoftButton
                  variant="gradient"
                  color="info"
                  size="small"
                  sx={{ marginRight: 1, height: "46px" }}
                  startIcon={<FilterList />}
                  onClick={handleClickOpen}
                >
                  Filter
                </SoftButton>
                <Dialog open={open} onClose={handleClose1} maxWidth="md">
                  {/* <DialogTitle>Filter</DialogTitle> */}
                  <DialogContent sx={{ width: 500 }}>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        gap: "20px",
                        padding: "47px",
                      }}
                    >
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <SoftTypography
                          style={{ marginRight: "20px", minWidth: "120px", fontSize: "1rem" }}
                        >
                          Date Range
                        </SoftTypography>
                        <SoftInputDateTime
                          // label="Start Date"
                          value={startDate}
                          onChange={(e) => setStartDate(e.target.value)}
                          sx={{ width: "500px !important" }}
                        />
                      </div>
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <SoftTypography
                          style={{ marginRight: "20px", minWidth: "120px", fontSize: "1rem" }}
                        >
                          Job ID
                        </SoftTypography>
                        <Autocomplete
                          disablePortal
                          id="name-autocomplete"
                          options={top100Films}
                          sx={{ width: "100%" }}
                          renderInput={(params) => <TextField {...params} placeholder="Job ID" />}
                        />
                      </div>
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <SoftTypography
                          style={{ marginRight: "20px", minWidth: "120px", fontSize: "1rem" }}
                        >
                          Client
                        </SoftTypography>
                        <Autocomplete
                          disablePortal
                          id="age-autocomplete"
                          options={clientdrop}
                          sx={{ width: "100%" }}
                          renderInput={(params) => <TextField {...params} placeholder="Client" />}
                        />
                      </div>
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <SoftTypography
                          style={{ marginRight: "20px", minWidth: "120px", fontSize: "1rem" }}
                        >
                          Status
                        </SoftTypography>
                        <Autocomplete
                          disablePortal
                          id="location-autocomplete"
                          options={statusdropdown}
                          sx={{ width: "100%" }}
                          renderInput={(params) => <TextField {...params} placeholder="Status" />}
                        />
                      </div>
                    </div>
                  </DialogContent>
                  <DialogActions>
                    <SoftButton variant="gradient" color="info" onClick={handleClose1}>
                      Reset
                    </SoftButton>
                    <SoftButton variant="gradient" color="info" onClick={handleApply}>
                      Apply
                    </SoftButton>
                  </DialogActions>
                </Dialog>
              </div>
            </Stack>
          </SoftBox>
          <DataTable
            table={dataHireFreshers}
            entriesPerPage={{
              defaultValue: 7,
              entries: [5, 7, 10, 15, 20, 25],
            }}
          />
        </Card>
      </SoftBox>
      <Footer />
    </DashboardLayout>
  );
}

export default HireFreshers;
