/**
=========================================================
* Soft UI Dashboard PRO- v4.0.2
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard-pro-react
* Copyright 2023 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

/* eslint-disable react/prop-types */
// Soft UI Dashboard PROcomponents
import SoftBadge from "components/SoftBadge";

// ProductsList page components
import ProductCell from "layouts/ecommerce/products/products-list/components/ProductCell";
// import ActionCell from "layouts/ecommerce/products/products-list/components/ActionCell";

// Images
// import adidasHoodie from "assets/images/ecommerce/adidas-hoodie.jpeg";
import macBookPro from "assets/images/ecommerce/macbook-pro.jpeg";
import JobCell from "../components/ActionCell/JobIcon";
// import metroChair from "assets/images/ecommerce/metro-chair.jpeg";
// import alchimiaChair from "assets/images/ecommerce/alchimia-chair.jpeg";
// import fendiCoat from "assets/images/ecommerce/fendi-coat.jpeg";
// import offWhiteJacket from "assets/images/ecommerce/off-white-jacket.jpeg";
// import yohjiYamamoto from "assets/images/ecommerce/yohji-yamamoto.jpeg";
// import mcqueenShirt from "assets/images/ecommerce/mcqueen-shirt.jpeg";
// import yellowChair from "assets/images/ecommerce/yellow-chair.jpeg";
// import heronTshirt from "assets/images/ecommerce/heron-tshirt.jpeg";
// import livingChair from "assets/images/ecommerce/living-chair.jpeg";
// import orangeSofa from "assets/images/ecommerce/orange-sofa.jpeg";
// import burberry from "assets/images/ecommerce/burberry.jpeg";
// import dgSkirt from "assets/images/ecommerce/d&g-skirt.jpeg";
// import undercover from "assets/images/ecommerce/undercover.jpeg";
// import { Description, PlayCircleFilled } from "@mui/icons-material";
// import DownloadIcon from "../components/ActionCell/DownloadIcon";

// Badges
const outOfStock = (
  <SoftBadge variant="contained" color="error" size="xs" badgeContent="inactive" container />
);
const inStock = (
  <SoftBadge variant="contained" color="success" size="xs" badgeContent="active" container />
);

const ActiveDataTable = {
  columns: [
    {
      Header: "Job Title",
      accessor: "jobTitle",
      width: "17%",
      Cell: ({ value: [name, data] }) => (
        <ProductCell image={data.image} name={name} checked={data.checked} />
      ),
    },
    { Header: "Client", accessor: "client" , width:"15%" },
    { Header: "Submitted", accessor: "submitted",width: "15%", },
    { Header: "Placed", accessor: "placed" , width:"15%" },

    { Header: "External App", accessor: "externalApp", width:"15%"  },
    // { Header: "video", accessor: "video" , width:"15%" },
    // { Header: "grade", accessor: "grade" , width:"15%" },
    {
      Header: "status",
      accessor: "status",
      Cell: ({ value }) => (value === "active" ? inStock : outOfStock),
    },
    { Header: "action", accessor: "action" },
  ],

  rows: [
    {
      jobTitle: ["UI/UX Designer",{ image: macBookPro, checked: true }],
      client: "JobQuest Network",
      submitted: 0,
      placed: 0,
      externalApp: 0,
      status: "active",
      action: <JobCell />,
    },
    {
      jobTitle: ["Software Developer",{ image: macBookPro, checked: false }],
      client: "TalentLink Pro",
      submitted: 0,
      placed: 0,
      externalApp: 0,
      status: "inactive",
      action: <JobCell />,
    },
    // {
    //   name: ["Prabha",{ image: macBookPro, checked: true }],
    //   contactPerson: "bala",
    //   email: "prabha@gmail.com",
    //   mobileNo: 9876543210,
    //   status: "active",
    //   action: <ActionCell />,
    // },
    // {
    //   name: ["Mani",{ image: macBookPro, checked: false }],
    //   contactPerson: "bala",
    //   email: "mani@gmail.com",
    //   mobileNo: 9876543210,
    //   status: "inactive",
    //   action: <ActionCell />,
    // },
    // {
    //   candidateName: ["MacBook Pro", { image: macBookPro, checked: true }],
    //   email: "Electronics",
    //   mobileNo: "$1,869",
    //   employability: 877712,
    //   resume: 0,
    //   video:"27",
    //   grade:"12",
    //   status: "out of stock",
    //   action: <ActionCell />,
    // },
    // {
    //   candidateName: ["Metro Bar Stool", { image: metroChair, checked: false }],
    //   email: "Furniture",
    //   mobileNo: "$99",
    //   employability: "0134729",
    //   resume: 978,
    //   video:"27",
    //   grade:"12",
    //   status: "in stock",
    //   action: <ActionCell />,
    // },
    // {
    //   candidateName: ["Alchimia Chair", { image: alchimiaChair, checked: false }],
    //   email: "Furniture",
    //   mobileNo: "$2,999",
    //   employability: 113213,
    //   resume: 0,
    //   video:"27",
    //   grade:"12",
    //   status: "out of stock",
    //   action: <ActionCell />,
    // },
    // {
    //   candidateName: ["Fendi Gradient Coat", { image: fendiCoat, checked: false }],
    //   email: "Clothing",
    //   mobileNo: "$869",
    //   employability: 634729,
    //   resume: 725,
    //   video:"27",
    //   grade:"12",
    //   status: "in stock",
    //   action: <ActionCell />,
    // },
    // {
    //   candidateName: ["Off White Cotton Bomber", { image: offWhiteJacket, checked: false }],
    //   email: "Clothing",
    //   mobileNo: "$1,869",
    //   employability: 634729,
    //   resume: 725,
    //   video:"27",
    //   grade:"12",
    //   status: "in stock",
    //   action: <ActionCell />,
    // },
    // {
    //   candidateName: ["Y-3 Yohji Yamamoto", { image: yohjiYamamoto, checked: true }],
    //   email: "Shoes",
    //   mobileNo: "$869",
    //   employability: 634729,
    //   resume: 725,
    //   video:"27",
    //   grade:"12",
    //   status: "in stock",
    //   action: <ActionCell />,
    // },
    // {
    //   candidateName: ["Alexander McQueen", { image: mcqueenShirt, checked: true }],
    //   email: "Clothing",
    //   mobileNo: "$1,199",
    //   employability: "00121399",
    //   resume: 51293,
    //   video:"27",
    //   grade:"12",
    //   status: "in stock",
    //   action: <ActionCell />,
    // },
    // {
    //   candidateName: ["Luin Floor Lamp", { image: yellowChair, checked: true }],
    //   email: "Furniture",
    //   mobileNo: "$1,900",
    //   employability: 434729,
    //   resume: 34,
    //   video:"27",
    //   grade:"12",
    //   status: "in stock",
    //   action: <ActionCell />,
    // },
    // {
    //   candidateName: ["Heron Preston T-shirt", { image: heronTshirt, checked: false }],
    //   email: "Clothing",
    //   mobileNo: "$149",
    //   employability: 928341,
    //   resume: 0,
    //   video:"27",
    //   grade:"12",
    //   status: "out of stock",
    //   action: <ActionCell />,
    // },
    // {
    //   candidateName: ["Gray Living Chair", { image: livingChair, checked: true }],
    //   email: "Furniture",
    //   mobileNo: "$2,099",
    //   employability: 9912834,
    //   resume: 32,
    //   video:"27",
    //   grade:"12",
    //   status: "in stock",
    //   action: <ActionCell />,
    // },
    // {
    //   candidateName: ["Derbyshire Orange Sofa", { image: orangeSofa, checked: false }],
    //   email: "Furniture",
    //   mobileNo: "$2,999",
    //   employability: 561151,
    //   resume: 22,
    //   video:"27",
    //   grade:"12",
    //   status: "in stock",
    //   action: <ActionCell />,
    // },
    // {
    //   candidateName: ["Burberry Low-Tops", { image: burberry, checked: true }],
    //   email: "Shoes",
    //   mobileNo: "$869",
    //   employability: 634729,
    //   resume: 725,
    //   video:"27",
    //   grade:"12",
    //   status: "in stock",
    //   action: <ActionCell />,
    // },
    // {
    //   candidateName: ["Dolce & Gabbana Skirt", { image: dgSkirt, checked: false }],
    //   email: "Designer",
    //   mobileNo: "$999",
    //   employability: "01827391",
    //   resume: 0,
    //   video:"27",
    //   grade:"12",
    //   status: "out of stock",
    //   action: <ActionCell />,
    // },
    // {
    //   candidateName: ["Undercover T-shirt", { image: undercover, checked: true }],
    //   email: "Shoes",
    //   mobileNo: "$869",
    //   employability: 63472,
    //   resume: 725,
    //   video:"27",
    //   grade:"12",
    //   status: "in stock",
    //   action: <ActionCell />,
    // },
  ],
};

export default ActiveDataTable;
