import React from "react";
import PropTypes from "prop-types"; // Import PropTypes
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import TextField from "@mui/material/TextField";

function SoftInputDateTime({ label, value, onChange }) {
  return (
    <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
      <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
        <SoftTypography
          component="label"
          variant="caption"
          fontWeight="bold"
          textTransform="capitalize"
        >
          {label}
        </SoftTypography>
      </SoftBox>
      <TextField
      
        type="datetime-local"
        value={value}
        onChange={onChange}
        InputLabelProps={{ shrink: true }}
      />
    </SoftBox>
  );
}

// Define propTypes for SoftInputDateTime component
SoftInputDateTime.propTypes = {
  label: PropTypes.string.isRequired, // label prop is a required string
  value: PropTypes.string.isRequired, // value prop is a required string
  onChange: PropTypes.func.isRequired, // onChange prop is a required function
};

export default SoftInputDateTime;
