/* eslint-disable react/prop-types */
// Soft UI Dashboard PROcomponents
// import SoftBadge from "components/SoftBadge";

// ProductsList page components

// Images

// import CompanyTableIcon from "../components/ActionCell/CompanyTableIcon";

// Badges
// const outOfStock = (
//   <SoftBadge variant="contained" color="error" size="xs" badgeContent="inactive" container />
// );
// const inStock = (
//   <SoftBadge variant="contained" color="success" size="xs" badgeContent="active" container />
// );

const CompanyAddressData = {
  columns: [
    {
      Header: "Address Name",
      accessor: "addressName",
      width: "17%",
    },
    { Header: "Address ID", accessor: "addressID", width: "15%" },
    { Header: "Vat ID", accessor: "vatID", width: "15%" },
    { Header: "Tax ID", accessor: "taxID", width: "15%" },
    { Header: "Address", accessor: "address", width: "15%" },
    { Header: "Country", accessor: "country", width: "15%" },
    { Header: "Profile Status", accessor: "profileStatus", width: "15%" },
    { Header: "Action", accessor: "action" },
  ],

  rows: [
    // {
    //   addressName: "Address Name",
    //   addressID: "Address ID",
    //   vatID: "Vat ID",
    //   taxID: "Tax ID",
    //   address: "Address",
    //   country: "Country",
    //   profileStatus: "Profile Status",
    //   action: <CompanyTableIcon />,
    // },
    // {
    //   addressName: "Address Name",
    //   addressID: "Address ID",
    //   vatID: "Vat ID",
    //   taxID: "Tax ID",
    //   address: "Address",
    //   country: "Country",
    //   profileStatus: "Profile Status",
    //   action: <CompanyTableIcon />,
    // },
  ],
};

export default CompanyAddressData;
