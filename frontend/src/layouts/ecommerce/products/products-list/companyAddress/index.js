import Card from "@mui/material/Card";
import React, { useState } from "react";
import {
  Box,
  Typography,
  Dialog,
  DialogContent,
  TextField,
  Icon,
  DialogActions,
  Button,
} from "@mui/material";
// Assuming Soft UI Dashboard PRO React components and custom components are imported properly
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import SoftButton from "components/SoftButton";
import DataTable from "examples/Tables/DataTable";

import CompanyTableIcon from "./CompanyTableIcon";

import CompanyAddressData from "./companyAddressData";

function CompanyAddressList() {
  const [rows, setRows] = useState(CompanyAddressData.rows);
  const [AddressName, setAddressName] = useState("");
  const [AddressID, setAddressID] = useState("");
  const [VatID, setVatID] = useState("");
  const [TaxID, setTaxID] = useState("");
  const [Address, setAddress] = useState("");
  const [Country, setCountry] = useState("");
  const [ProfileStatus, setProfileStatus] = useState("");
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleAdd = () => {
    const newRow = {
      addressName: AddressName,
      addressID: AddressID,
      vatID: VatID,
      taxID: TaxID,
      address: Address,
      country: Country,
      profileStatus: ProfileStatus,
      action: <CompanyTableIcon onDelete={() => handleDelete(AddressID)} />,
    };
    setRows([...rows, newRow]);
    handleClose();
  };

  const handleDelete = (id) => {
    const updatedRows = rows.filter((row) => row.addressID !== id);
    setRows(updatedRows);
  };

  const columns = [
    { Header: "Address Name", accessor: "addressName", width: "17%" },
    { Header: "Address ID", accessor: "addressID", width: "15%" },
    { Header: "Vat ID", accessor: "vatID", width: "15%" },
    { Header: "Tax ID", accessor: "taxID", width: "15%" },
    { Header: "Address", accessor: "address", width: "15%" },
    { Header: "Country", accessor: "country", width: "15%" },
    { Header: "Profile Status", accessor: "profileStatus", width: "15%" },
    { Header: "Action", accessor: "action" },
  ];

  return (
    <Card id="work-information">
      <SoftBox p={3}>
        <SoftTypography variant="h5">Work Information</SoftTypography>
      </SoftBox>
      <DataTable table={{ columns, rows }} />

      <SoftBox ml={{ xs: 0, sm: 5 }} mt={{ xs: 2, sm: 2 }} mb={{ xs: 2, sm: 5 }}>
        <SoftButton variant="gradient" color="info" size="small" onClick={handleClickOpen}>
          <Icon sx={{ fontWeight: "bold" }}>add</Icon>&nbsp; Add New
        </SoftButton>
        <Dialog open={open} onClose={handleClose} maxWidth="md">
          <DialogContent sx={{ width: 500 }}>
            <Box sx={{ display: "flex", flexDirection: "column", gap: 3, p: 2 }}>
              <Typography variant="h5" sx={{ textAlign: "center" }}>
                Work Information
              </Typography>
              <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>Address Name</Typography>
                <TextField
                  fullWidth
                  placeholder="Address Name"
                  value={AddressName}
                  onChange={(e) => setAddressName(e.target.value)}
                />
              </Box>
              <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>Address ID</Typography>
                <TextField
                  fullWidth
                  placeholder="Address ID"
                  value={AddressID}
                  onChange={(e) => setAddressID(e.target.value)}
                />
              </Box>
              <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>VAT ID</Typography>
                <TextField
                  fullWidth
                  placeholder="VAT ID"
                  value={VatID}
                  onChange={(e) => setVatID(e.target.value)}
                />
              </Box>
              <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>Tax ID</Typography>
                <TextField
                  fullWidth
                  placeholder="TAX ID"
                  value={TaxID}
                  onChange={(e) => setTaxID(e.target.value)}
                />
              </Box>
              <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>Address</Typography>
                <TextField
                  fullWidth
                  placeholder="Address"
                  value={Address}
                  onChange={(e) => setAddress(e.target.value)}
                />
              </Box>
              <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>Country</Typography>
                <TextField
                  fullWidth
                  placeholder="Country"
                  value={Country}
                  onChange={(e) => setCountry(e.target.value)}
                />
              </Box>
              <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                <Typography sx={{ minWidth: 120, fontSize: "1rem" }}>Profile Status</Typography>
                <TextField
                  fullWidth
                  placeholder="Profile Status"
                  value={ProfileStatus}
                  onChange={(e) => setProfileStatus(e.target.value)}
                />
              </Box>
            </Box>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>Cancel</Button>
            <Button onClick={handleAdd}>Add</Button>
          </DialogActions>
        </Dialog>
      </SoftBox>
    </Card>
  );
}

export default CompanyAddressList;
