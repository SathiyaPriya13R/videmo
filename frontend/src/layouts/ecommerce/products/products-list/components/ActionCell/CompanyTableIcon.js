import React from "react";
import PropTypes from "prop-types";
import { IconButton } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";

function CompanyTableIcon({ onDelete }) {
  return (
    <IconButton onClick={onDelete}>
      <DeleteIcon />
    </IconButton>
  );
}

// Adding prop types validation
CompanyTableIcon.propTypes = {
  onDelete: PropTypes.func.isRequired,
};

export default CompanyTableIcon;
