import { useState } from "react";

// @mui/material components
import Card from "@mui/material/Card";
import Switch from "@mui/material/Switch";
import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";

// Soft UI Dashboard PRO components
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";

function PlatformNew() {
  const [followsMe, setFollowsMe] = useState(true);
  const [answersPost, setAnswersPost] = useState(false);
  const [mentionsMe, setMentionsMe] = useState(true);
  const [newLaunches, setNewLaunches] = useState(false);
  const [productUpdate, setProductUpdate] = useState(true);
  const [newsletter, setNewsletter] = useState(true);

  // State for the new questions
  const [questions, setQuestions] = useState(Array(10).fill(false));

  const handleQuestionChange = (index) => {
    const newQuestions = [...questions];
    newQuestions[index] = !newQuestions[index];
    setQuestions(newQuestions);
  };

  const questionTexts = [
    "Do you like our product?",
    "Is our website easy to navigate?",
    "Are you satisfied with our customer service?",
    "Would you recommend our product to others?",
    "Do you find our pricing reasonable?",
    "Is the product quality up to your expectations?",
    "Do you find our product features useful?",
    "Are you likely to purchase from us again?",
    "Is the product description accurate?",
    "Are you satisfied with the delivery time?",
  ];

  return (
    <Card>
      <SoftBox pt={2} px={2}>
        <SoftBox mt={3}>
          <SoftTypography
            variant="caption"
            fontWeight="bold"
            color="text"
            textTransform="uppercase"
          >
            Basic
          </SoftTypography>
        </SoftBox>
        <SoftBox
          style={{ maxHeight: "420px", overflowY: "auto" }} // Set a max height and enable scroll
        >
          {questionTexts.map((questionText, index) => (
            <SoftBox display="flex" py={1} mb={0.25} key={index}>
              <SoftBox mt={0.25}>
                <Checkbox checked={questions[index]} onChange={() => handleQuestionChange(index)} />
              </SoftBox>
              <SoftBox width="80%" ml={2}>
                <SoftTypography variant="button" fontWeight="regular" color="text">
                  {questionText}
                </SoftTypography>
              </SoftBox>
            </SoftBox>
          ))}
        </SoftBox>
      </SoftBox>
    </Card>
  );
}



export default PlatformNew;

