
import Card from "@mui/material/Card";
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import Grid from "@mui/material/Grid";
import FormField from "layouts/pages/account/components/FormField";
import { useProfileUIController } from "context";
import { setUpdateProfile } from "context";
import { useState } from "react";


function Notifications() {
  const [controller, dispatch] = useProfileUIController();
  const [socialdetails, setsocialdetails] = useState ({
    linkedin:"" , twitter:"", facebook:"", instagram:""
    });
  const handlechange = (name, value) => {
         setsocialdetails(prevdata => ({...prevdata ,[name]: value}));
         setUpdateProfile(dispatch, {socialdetails: {...socialdetails, [name]: value}});
  };
  return (
    <Card id="social-information">
      <SoftBox p={3} lineHeight={1}>
        <SoftBox mb={1}>
          <SoftTypography variant="h5">Social Information</SoftTypography>
        </SoftBox>

      </SoftBox>
   
      <SoftBox component="form" pb={3} px={3}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <FormField label="Linked In" placeholder="Linked In" value= {socialdetails.linkedin} onChange={(e) => handlechange("linkedin" , e.target.value)}/>
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField label="Instagram" placeholder="Instagram" value= {socialdetails.instagram} onChange={(e) => handlechange("instagram" , e.target.value)}/>
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField label="X" placeholder="X" value= {socialdetails.twitter} onChange={(e) => handlechange("twitter" , e.target.value)}/>
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField label="Facebook" placeholder="Facebook" value= {socialdetails.facebook} onChange={(e) => handlechange("facebook" , e.target.value)}/>
          </Grid>
        </Grid>
      
      </SoftBox>
    </Card>
  );
}

export default Notifications;
