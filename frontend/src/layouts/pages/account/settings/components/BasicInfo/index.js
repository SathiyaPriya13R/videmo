import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import Card from "@mui/material/Card";
import Grid from "@mui/material/Grid";
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import FormField from "layouts/pages/account/components/FormField";
import SoftSelect from "components/SoftSelect"; // Assuming this is your custom select component
import { GetCountries, GetState, GetCity } from "react-country-state-city";
// import SoftInputDateTime from "layouts/ecommerce/products/products-list/activeJobs/SoftInputDateTime";
import SoftInputDate from "layouts/ecommerce/products/products-list/activeJobs/SoftInputDate";
import { useProfileUIController } from "context";
import { setUpdateProfile } from "context";




function SoftInput({ label, onChange }) {
  const handleFileUpload = async (event) => {
    const file = event.target.files[0];
    if (file) {
      const formData = new FormData();
      formData.append("file", file);

      try {
        const response = await axios.post("http://localhost:5000/api/upload", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        });

        if (response.status === 200) {
          console.log("File uploaded successfully:", response.data);
          // Pass the parsed data to the parent component
          onChange(response.data.parsed_data);
        } else {
          console.error("File upload failed:", response.statusText);
        }
      } catch (error) {
        if (error.response) {
          // Server responded with a status other than 200 range
          console.error("Server error:", error.response.data);
        } else if (error.request) {
          // Request was made but no response was received
          console.error("Network error:", error.request);
        } else {
          // Something else happened
          console.error("Error:", error.message);
        }
      }
    }
  };

  return (
    <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
      <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
        <SoftTypography
          component="label"
          variant="caption"
          fontWeight="bold"
          textTransform="capitalize"
        >
          {label}
        </SoftTypography>
      </SoftBox>
      <input
        type="file"
        accept=".pdf"
        onChange={handleFileUpload}
        style={{ padding: "8px", border: "1px solid #ccc", borderRadius: "4px" }}
      />
    </SoftBox>
  );
}

// Define propTypes for SoftInput
SoftInput.propTypes = {
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

function BasicInfo() {
  const [controller, dispatch] = useProfileUIController();
  const {name} = controller
  const [parsedData, setParsedData] = useState({
    firstname: "",
    contact_number: "",
    alt_contact_number: "",
    middleName:"",
    lastName:"",
    email: "",
    gender: "",
    linkDate: "",
    confirmationEmail: "",
    techskills:"",
    skills: [],
    education: [],
    hobbies: [],
    indian_states: [],
    linkedin_ids: [],
    linkedin_urls: [],
    country: "",
    state: "",
    city: "",
    noticeperiod: "",
    visastatus: "",
    visatype:"",
    jobtitle:"",
    location: "",
  });
  const [countryCode, setCountryCode] = useState(0);
  const [countryid, setCountryid] = useState(0);
  const [stateid, setStateid] = useState(0);
  const [countryName, setCountryName] = useState("select a country");
  const [stateName, setStateName] = useState("select a State");
  const [cityid, setCityid] = useState(0);
  const [cityName, setCityName] = useState("select a City");
  const [countriesList, setCountriesList] = useState([]);
  const [stateList, setStateList] = useState([]);
  const [cityList, setCityList] = useState([]);
  const [linkDate, setLinkDate] = useState("");

  useEffect(() => {
    GetCountries().then((result) => {
      setCountriesList(result);
      console.log(result);
    });
  }, [countryid,stateid,countryName,stateName,cityid,cityName,linkDate]);

  const handleParsedDataChange = (data) => {
    setParsedData({
      ...parsedData,
      firstname: data.firstname || "",
      contact_number: data.contact_number || "",
      alt_contact_number: data.alt_contact_number || "",
      email: data.email || "",
      confirmationEmail: data.email || "",
      skills: data.skills || [],
      education: data.education || [],
      hobbies: data.hobbies || [],
      indian_states: data.indian_states || [],
      linkedin_ids: data.linkedin_ids || [],
      linkedin_urls: data.linkedin_urls || [],
      middleName: data.middleName ||"",
      lastName: data.lastName ||"",
      gender: data.gender ||"",
      linkDate: data.linkDate ||"",
      techskills: data.techskills ||"",
      country: data.country ||"",
      state: data.state ||"",
      city: data.city ||"",
      noticeperiod:  data.noticeperiod||"",
      visastatus: data.visastatus ||"",
      visatype: data.visatype ||"",
      jobtitle: data.jobtitle ||"",
      location: data.location ||"",
    });
    setUpdateProfile(dispatch, {firstname: data.firstname || "",
    contact_number: data.contact_number || "",
    alt_contact_number: data.alt_contact_number || "",
    email: data.email || "",
    confirmationEmail: data.email || "",
    skills: data.skills || [],
    education: data.education || [],
    hobbies: data.hobbies || [],
    indian_states: data.indian_states || [],
    linkedin_ids: data.linkedin_ids || [],
    linkedin_urls: data.linkedin_urls || [],
    middleName: data.middleName ||"",
    lastName: data.lastName ||"",
    gender: data.gender ||"",
    linkDate: data.linkDate ||"",
    techskills: data.techskills ||"",
    country: data.country ||"",
    state: data.state ||"",
    city: data.city ||"",
    noticeperiod:  data.noticeperiod||"",
    visastatus: data.visastatus ||"",
    visatype: data.visatype ||"",
    jobtitle: data.jobtitle ||"",
    location: data.location ||"",})
  };

  const handleInputChange = (fname, value) => {
    setParsedData((prevData) => ({
      ...prevData,
      [fname]: value,
    }));
    setUpdateProfile(dispatch, {[fname]: value})
    console.log(name)
  };

  const handleCountryChange = (selectedOption) => {
    const country = countriesList[selectedOption];
    console.log(country,"country");
    setCountryCode(country.id)
    setCountryid(country.name);
    setCountryName(country.name);
    setParsedData((prevData) => ({
      ...prevData,
      country: country.name,
      state: "",
      city: "",
    }));

    GetState(country.id).then((result) => {
      setStateList(result);
      setCityList([]); // Clear city list when country changes
      setStateid(0); // Reset state selection
      setCityid(0); // Reset city selection
    });
  };
  
  const handleStateChange = (selectedOption) => {
    //const state = stateList[selectedOption];
    setStateid(selectedOption);
    const state =stateList.find((state) => state.id === selectedOption).name
    setStateName(state);
    setParsedData((prevData) => ({
      ...prevData,
      state: selectedOption,
      city: "",
    }));
    GetCity(countryCode, selectedOption).then((result) => {
      console.log(countryCode, selectedOption,"city");
      setCityList(result);
      setCityid(0); // Reset city selection
    });
  };
  
  const handleCityChange = (selectedOption) => {
    const city = cityList[selectedOption];
    const cityname = cityList[selectedOption].name;
    console.log(city.name,"city12")
    setCityid(city.id);
    setCityName(cityname);
    setParsedData((prevData) => ({
      ...prevData,
      city: city.name,
    }));
  };

  const selectData = {
    gender: ["Male", "Female", "Other"].map((gender, index) => ({ value: index, label: gender })),
    birthDate: [
      "January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ].map((month, index) => ({ value: index, label: month })),
    days: Array.from({ length: 31 }, (_, i) => ({ value: i + 1, label: i + 1 })),
    years: Array.from({ length: 100 }, (_, i) => ({ value: 2024 - i, label: 2024 - i })),
    visatype: ["Platinum", "Gold", "Silver"].map((visatype, index) => ({ value: index, label: visatype })),
    visastatus: ["Complete", "Incomplate"].map((visastatus, index) => ({ value: index, label: visastatus })),

  };

  return (
    <Card id="basic-info" sx={{ overflow: "visible" }}>
      <SoftBox p={3}>
        <SoftTypography variant="h5">Basic Info</SoftTypography>
      </SoftBox>
      <SoftBox component="form" pb={3} px={3}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Grid container spacing={3}>
              <Grid item xs={12} md={4}>
                <SoftInput label="Resume" onChange={handleParsedDataChange} />
              </Grid>
              <Grid item xs={12} md={4}>
                <SoftInput label="Identification Doc (Adhaar)" onChange={() => {}} />
              </Grid>
              <Grid item xs={12} md={4}>
                <SoftInput label="Identification Doc (PAN)" onChange={() => {}} />
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12} sm={4}>
            <FormField
              label="first name"
              placeholder="Alec"
              value={parsedData.firstname}
              onChange={(e) => handleInputChange("firstname", e.target.value)}
            />
          </Grid>


          <Grid item xs={12} sm={4}>
            <FormField
              label="middle name"
              placeholder="A"
              value={parsedData.middleName || ""}
              onChange={(e) => handleInputChange("middleName", e.target.value)}
            />
          </Grid>


          <Grid item xs={12} sm={4}>
            <FormField
              label="last name"
              placeholder="Thompson"
              value={parsedData.lastName || ""}
              onChange={(e) => handleInputChange("lastName", e.target.value)}
            />
          </Grid>


          <Grid item xs={12}>
            <Grid container spacing={3}>
              <Grid item xs={12} sm={4}>
                <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
                  <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                    <SoftTypography component="label" variant="caption" fontWeight="bold" textTransform="capitalize">
                      Gender
                    </SoftTypography>
                  </SoftBox>
                  <SoftSelect
                    placeholder="Select Gender"
                    options={selectData.gender}
                    onChange={(selectedOption) => handleInputChange("gender", selectData.gender[selectedOption.value].label)}
                    value={selectData.gender.find((option) => option.label === parsedData.gender)}
                  />
                </SoftBox>
              </Grid>
              <Grid item xs={12} sm={4}>
                <SoftBox>
                  <SoftInputDate
                    label="Select Date of Birth"
                    value={parsedData.linkDate}
                    onChange={(e) => { setLinkDate(e.target.value)
                      handleInputChange("linkDate", e.target.value)
                    }
                    }
                  />
                </SoftBox>
              </Grid>
              <Grid item xs={12} sm={4}>
            <FormField value={parsedData.location} onChange={(e) => handleInputChange("location", e.target.value)}
                    label="Your Location" placeholder="Current Location" />
          </Grid>
              </Grid>
              </Grid>
          <Grid item xs={12} sm={4}>
            <FormField
              label="email"
              placeholder="example@email.com"
              name="email"
              value={parsedData.email}
              onChange={(e) => handleInputChange("email", e.target.value)}
              inputProps={{ type: "email" }}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <FormField
              label="confirmation email"
              placeholder="example@email.com"
              name="confirmationEmail"
              value={parsedData.confirmationEmail}
              onChange={(e) => handleInputChange("confirmationEmail", e.target.value)}
              inputProps={{ type: "email" }}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <FormField
              label="phone number"
              placeholder="+40 735 631 620"
              inputProps={{ type: "number" }}
              value={parsedData.contact_number}
              onChange={(e) => handleInputChange("contact_number", e.target.value)}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <FormField
              label="Alternate phone number"
              placeholder="+40 735 631 620"
              inputProps={{ type: "number" }}
              value={parsedData.alt_contact_number}
              onChange={(e) => handleInputChange("alt_contact_number", e.target.value)}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <FormField value={parsedData.techskills}label="Technical Skills" placeholder="Skill Name" 
                          onChange={(e) => handleInputChange("techskills", e.target.value)}
                          />
          </Grid>
          <Grid value={parsedData.jobtitle} item xs={12} sm={4}>
            <FormField label="Job Title" placeholder="Job Title"
        onChange={(e) => handleInputChange("jobtitle", e.target.value)}
                                      />
          </Grid>
          <Grid item xs={12} sm={4}>
          <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
                  <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                    <SoftTypography component="label" variant="caption" fontWeight="bold" textTransform="capitalize">
                      Visa Type
                    </SoftTypography>
                  </SoftBox>
                  <SoftSelect
                    placeholder="Select Type"
                    options={selectData.visatype}
                    onChange={(selectedOption) => handleInputChange("visatype", selectData.visatype[selectedOption.value].label)}
                    value={selectData.visatype.find((option) => option.label === parsedData.visatype)}
                  />
                </SoftBox>      
                  </Grid>
                  <Grid item xs={12} sm={4}>
          <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
                  <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                    <SoftTypography component="label" variant="caption" fontWeight="bold" textTransform="capitalize">
                      Visa Status
                    </SoftTypography>
                  </SoftBox>
                  <SoftSelect
                    placeholder="Select status"
                    options={selectData.visastatus}
                    onChange={(selectedOption) => handleInputChange("visastatus", selectData.visastatus[selectedOption.value].label)}
                    value={selectData.visastatus.find((option) => option.label === parsedData.visastatus)}
                  />
                </SoftBox>      
                  </Grid>
          <Grid value={parsedData.noticeperiod} item xs={12} sm={4}>
            <FormField label="Notice Period" placeholder="Notice Period" 
                    onChange={(e) => handleInputChange("noticeperiod", e.target.value)}
                    />
          </Grid>
          <Grid item xs={12} sm={4}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SoftTypography component="label" variant="caption" fontWeight="bold" textTransform="capitalize">
                  Country
                </SoftTypography>
              </SoftBox>
             
              <SoftSelect
                  // placeholder= {countryName}
                  options={countriesList.map((country, index) => ({
                    value: country.id,
                    label: country.name,
                  }))}
                  onChange={(selectedOption) => {
                    handleCountryChange(selectedOption.value);
                    handleInputChange("country", selectedOption);
                  }}
                  // value={countriesList.find((option) => option.id === parsedData.id)}
                  value={parsedData.country} // Make sure countryid is set correctly
                />
            </SoftBox>
          </Grid>
          <Grid item xs={12} sm={4}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SoftTypography component="label" variant="caption" fontWeight="bold" textTransform="capitalize">
                  State
                </SoftTypography>
              </SoftBox>
              <SoftSelect
                placeholder = "select a state"
                options={stateList.map((state, index) => ({
                  value: state.id,
                  label: state.name,
                }))}
                onChange={(selectedOption) => {
                  handleStateChange(selectedOption.value);
                  handleInputChange("state",  selectedOption);
                }}
                value={parsedData.state}
              />
            </SoftBox>
            
          </Grid>
          <Grid item xs={12} sm={4}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SoftTypography component="label" variant="caption" fontWeight="bold" textTransform="capitalize">
                  City
                </SoftTypography>
              </SoftBox>
              <SoftSelect
                placeholder="select a city"
                options={cityList.map((city, index) => ({
                  value: index,
                  label: city.name,
                }))}
                onChange={(selectedOption) => {
                  handleCityChange(selectedOption.value);
                  handleInputChange("city", selectedOption);
                }}
                value={parsedData.city}
              />
            </SoftBox>
          </Grid>
        </Grid>
      </SoftBox>
    </Card>
  );
}

export default BasicInfo;
