import React, { useEffect, useState } from "react";
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import { Card, Grid, InputAdornment, Box, Radio, FormControlLabel } from "@mui/material";
import FormField from "../../components/FormField";
import SoftSelect from "components/SoftSelect";
import SoftButton from "components/SoftButton";
import DataTable from "examples/Tables/DataTable";
import BankInformationData from "./BankInformationData";
// import {useAgencyUIController} from "context";
// import { setUpdateAgency } from "context";


 
function Business() {
  const [businessdata, setBusinessData] = useState({
    yearfounded: "",
    numberofemployes: "",
    networkid: "",
    annuvarevenue: "",
    stocksymbol: "",
    supplierlegalform: "",
    globallocationnumber: "",
    dun50number: "",
    texclassification: "",
    taxtype: "",
    taxid: "",
    statetaxid:"",
    regionaltaxid: "",
    gstrequired: "",
    gstvatid: "",
    gstvatdocuments: "",
    taxclearenced: "",
    taxclearencenumber: "",
    texclearencedocument: "",
    bankinformation:[],
    bussinesstype: "",
  });


  // const [controller, dispatch] = useAgencyUIController();
  const [selectedValue, setSelectedValue] = useState("a");
  const [file, setFile] = useState(null);
  useEffect(() => {
    // Your side effect logic here
    console.log("One of the fields was updated");
  }, [file,setBusinessData]);
  const handleChange = (event) => {
    setSelectedValue(event.target.value);
  };
  const handleFileChange = (event) => {
    setFile(event.target.files[0]);
  };
 
  return (
    <SoftBox mt={5}>
      <Card sx={{ padding: "35px" }}>
        <SoftTypography
          variant="h6"
          mb={2}
          sx={{ marginLeft: "3px", marginTop: "30px", marginBottom: "30px" }}
        >
          Business Information
        </SoftTypography>
 
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <FormField label="Year Founded" placeholder="Year Founded" value = {businessdata.yearfounded}/>
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField label="Number Of Employees" placeholder="Number Of Employees" value = {businessdata.numberofemployes}/>
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField label="Network ID" placeholder="Network ID" value = {businessdata.networkid}/>
          </Grid>
          <Grid item xs={12} sm={6}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SoftTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Annual Revenue
                </SoftTypography>
              </SoftBox>
              <SoftSelect
                placeholder="Annual Revenue"
                options={[
                  {
                    value: "Sales-focused revenue analysis",
                    label: "Sales-focused revenue analysis",
                  },
                  {
                    value: "Revenue analysis by department",
                    label: "Revenue analysis by department",
                  },
                  { value: "Revenue analysis by product", label: "Revenue analysis by product" },
                ]}
                value = {businessdata.annuvarevenue}/>
            </SoftBox>
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField label="Stock Symbol" placeholder="Stock Symbol" value = {businessdata.stocksymbol}/>
          </Grid>
        </Grid>
      </Card>
 
      <Card sx={{ padding: "35px", marginTop: "40px" }}>
        <SoftTypography
          variant="h6"
          mb={2}
          sx={{ marginLeft: "3px", marginTop: "30px", marginBottom: "30px" }}
        >
          Financial Information
        </SoftTypography>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SoftTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Supplier Legal form
                </SoftTypography>
              </SoftBox>
              <SoftSelect
                placeholder="Supplier Legal form"
                options={[
                  { value: "India", label: "India" },
                  { value: "Asia", label: "Austria" },
                  { value: "USA", label: "USA" },
                ]}
              />
            </SoftBox>
          </Grid>
          <Grid item xs={12} sm={6}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SoftTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  D-U-N_50 Number
                </SoftTypography>
              </SoftBox>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={4}>
                  <FormField
                    placeholder="0"
                    inputType="number"
                    InputProps={{
                      endAdornment: <InputAdornment position="end">d</InputAdornment>,
                      inputProps: { min: 0 },
                    }}
                  />
                </Grid>
                <Grid item xs={12} sm={4}>
                  <FormField
                    placeholder="0"
                    inputType="number"
                    InputProps={{
                      endAdornment: <InputAdornment position="end">h</InputAdornment>,
                      inputProps: { min: 0, max: 23 },
                    }}
                  />
                </Grid>
                <Grid item xs={12} sm={4}>
                  <FormField
                    placeholder="0"
                    inputType="number"
                    InputProps={{
                      endAdornment: <InputAdornment position="end">m</InputAdornment>,
                      inputProps: { min: 0, max: 59 },
                    }}
                  />
                </Grid>
              </Grid>
            </SoftBox>
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField label="Global Location Number" placeholder="Global Location Number" value = {businessdata.globallocationnumber}/>
          </Grid>
        </Grid>
      </Card>
 
      <Card sx={{ padding: "35px", marginTop: "40px" }}>
        <SoftTypography
          variant="h6"
          mb={2}
          sx={{ marginLeft: "3px", marginTop: "30px", marginBottom: "30px" }}
        >
          Tax Information
        </SoftTypography>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SoftTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Tax Classification
                </SoftTypography>
              </SoftBox>
              <SoftSelect
                placeholder="Tax Classification"
                options={[
                  { value: "India", label: "India" },
                  { value: "Asia", label: "Austria" },
                  { value: "USA", label: "USA" },
                ]}
              />
            </SoftBox>
          </Grid>
          <Grid item xs={12} sm={6}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SoftTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Taxation Type
                </SoftTypography>
              </SoftBox>
              <SoftSelect
                placeholder="Taxation Type"
                options={[
                  { value: "India", label: "India" },
                  { value: "Asia", label: "Austria" },
                  { value: "USA", label: "USA" },
                ]}
              />
            </SoftBox>
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField label="Tax ID" placeholder="Tax ID" value = {businessdata.taxid}/>
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField label="State Tax ID" placeholder="State Tax ID" value = {businessdata.taxtype}/>
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField label="Regional Tax ID" placeholder="Regional Tax ID" value = {businessdata.regionaltaxid}/>
          </Grid>
          <Grid item xs={12} sm={6}>
            <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
              <SoftTypography
                component="label"
                variant="caption"
                fontWeight="bold"
                textTransform="capitalize"
              >
                GST/Vat Registered
              </SoftTypography>
            </SoftBox>
            <Box sx={{ display: "flex", gap: 2, marginLeft: "20px" }}>
              <FormControlLabel
                control={
                  <Radio
                    checked={selectedValue === "a"}
                    onChange={handleChange}
                    value="a"
                    name="radio-buttons"
                    inputProps={{ "aria-label": "A" }}
                  />
                }
                label="Yes"
              />
              <FormControlLabel
                control={
                  <Radio
                    checked={selectedValue === "b"}
                    onChange={handleChange}
                    value="b"
                    name="radio-buttons"
                    inputProps={{ "aria-label": "B" }}
                  />
                }
                label="No"
              />
            </Box>
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField label="GST /Vat ID" placeholder="GST /Vat ID" value = {businessdata.gstvatid}/>
          </Grid>
          <Grid item xs={12} sm={6}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SoftTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  GST/VAT Registration Document
                </SoftTypography>
              </SoftBox>
              <FormField type="file" onChange={handleFileChange} />
            </SoftBox>
          </Grid>
          <Grid item xs={12} sm={6}>
            <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
              <SoftTypography
                component="label"
                variant="caption"
                fontWeight="bold"
                textTransform="capitalize"
              >
                Tax Clearance
              </SoftTypography>
            </SoftBox>
            <Box sx={{ display: "flex", gap: 2, marginLeft: "20px" }}>
              <FormControlLabel
                control={
                  <Radio
                    checked={selectedValue === "a"}
                    onChange={handleChange}
                    value="a"
                    name="radio-buttons"
                    inputProps={{ "aria-label": "A" }}
                  />
                }
                label="Yes"
              />
              <FormControlLabel
                control={
                  <Radio
                    checked={selectedValue === "b"}
                    onChange={handleChange}
                    value="b"
                    name="radio-buttons"
                    inputProps={{ "aria-label": "B" }}
                  />
                }
                label="No"
              />
            </Box>
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormField label="Tax Clearance No" placeholder="Tax Clearance No" value = {businessdata.taxclearenced}/>
          </Grid>
          <Grid item xs={12} sm={6}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SoftTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Tax Clearance Document
                </SoftTypography>
              </SoftBox>
              <FormField type="file" onChange={handleFileChange} />
            </SoftBox>
          </Grid>
        </Grid>
      </Card>
 
      <SoftBox my={3}>
        <Card>
          <SoftTypography
            variant="h6"
            mb={2}
            sx={{ marginLeft: "30px", marginTop: "30px", marginBottom: "30px" }}
          >
            Bank Information
          </SoftTypography>
          <DataTable
            table={BankInformationData}
            entriesPerPage={{
              defaultValue: 7,
              entries: [5, 7, 10, 15, 20, 25],
            }}
            canSearch
          />
          <SoftBox my={3}>
            <SoftButton variant="text" color="info">
              + Add New
            </SoftButton>
          </SoftBox>
        </Card>
      </SoftBox>
      {/* <BackInformationList /> */}
      <Card sx={{ padding: "35px", marginTop: "40px" }}>
        <SoftTypography
          variant="h6"
          mb={2}
          sx={{ marginLeft: "3px", marginTop: "30px", marginBottom: "30px" }}
        >
          Business Type
        </SoftTypography>
        <Grid item xs={12} sm={6}>
          <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
            <SoftBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
              <SoftTypography
                component="label"
                variant="caption"
                fontWeight="bold"
                textTransform="capitalize"
              >
                Business Type
              </SoftTypography>
            </SoftBox>
            <SoftSelect
              placeholder="Annual Revenue"
              options={[
                {
                  value: "Sales-focused revenue analysis",
                  label: "Sales-focused revenue analysis",
                },
                {
                  value: "Revenue analysis by department",
                  label: "Revenue analysis by department",
                },
                { value: "Revenue analysis by product", label: "Revenue analysis by product" },
              ]}
            />
          </SoftBox>
        </Grid>
        <SoftBox my={3}>
          <SoftButton variant="gradient" color="info" sx={{ marginRight: "15px" }}>
            Save
          </SoftButton>
          {/* <SoftButton variant="gradient" color="info">
            Cancel
          </SoftButton> */}
        </SoftBox>
      </Card>
    </SoftBox>
  );
}
 
export default Business;