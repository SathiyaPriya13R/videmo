import React from "react";
import PropTypes from "prop-types"; // Import PropTypes
import { IconButton } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
 
function CompanyTableIcon({ onDelete }) {
  return (
    <IconButton onClick={onDelete}>
      <DeleteIcon />
    </IconButton>
  );
}
 
// Define PropTypes for the onDelete prop
CompanyTableIcon.propTypes = {
  onDelete: PropTypes.func.isRequired, // onDelete should be a function and is required
};
 
export default CompanyTableIcon;