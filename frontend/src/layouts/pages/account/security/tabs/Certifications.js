import React, { useState } from "react";
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import FormField from "../../components/FormField";
import { Card, Grid } from "@mui/material";
import SoftButton from "components/SoftButton";
import axios from 'axios';

function Certifications() {
  const [certificateName, setCertificateName] = useState("");
  const [certificateFile, setCertificateFile] = useState(null);

  const handleFileChange = (e) => {
    setCertificateFile(e.target.files[0]);
  };

  const handleSubmit = async () => {
    const formData = new FormData();
    formData.append('name', certificateName);
    formData.append('file', certificateFile);

    try {
      await axios.post('http://localhost:5000/api/certificate/certificate', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      });
      console.log('Certificate saved successfully');
    } catch (error) {
      console.error('Error saving certificate:', error);
    }
  };

  return (
    <SoftBox mt={5}>
      <Card sx={{ padding: "35px" }}>
        <SoftTypography
          variant="h6"
          mb={2}
          sx={{ marginLeft: "3px", marginTop: "30px", marginBottom: "30px" }}
        >
          Certifications
        </SoftTypography>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <FormField
              label="Document Name"
              placeholder="Enter Document Name"
              value={certificateName}
              onChange={(e) => setCertificateName(e.target.value)}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox lineHeight={0} display="inline-block">
                <SoftTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Choose Document
                </SoftTypography>
              </SoftBox>
              <FormField type="file" onChange={handleFileChange} />
            </SoftBox>
          </Grid>
        
        </Grid>
        <SoftButton
          variant="gradient"
          color="info"
          sx={{ marginRight: "15px", width: "100px", marginTop: "40px" }}
          onClick={handleSubmit}
        >
          Save
        </SoftButton> 
      </Card>
      
    </SoftBox>
  );
}

export default Certifications;
