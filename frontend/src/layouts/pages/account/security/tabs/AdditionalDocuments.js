import React, { useState } from "react";
import SoftBox from "components/SoftBox";
import SoftTypography from "components/SoftTypography";
import FormField from "../../components/FormField";
import { Card, Grid } from "@mui/material";
import SoftButton from "components/SoftButton";
import axios from 'axios';

function AdditionalDocuments() {
  const [document1, setDocument1] = useState({ name: "", file: null });
  const [document2, setDocument2] = useState({ name: "", file: null });

  const handleFileChange = (e, setDocument) => {
    setDocument((prevState) => ({ ...prevState, file: e.target.files[0] }));
  };

  const handleSubmit = async () => {
    const formData1 = new FormData();
    formData1.append('name', document1.name);
    formData1.append('file', document1.file);

    const formData2 = new FormData();
    formData2.append('name', document2.name);
    formData2.append('file', document2.file);

    try {
      await axios.post('http://localhost:5000/api/additional_documents/additionaldoc', formData1, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      });
      await axios.post('http://localhost:5000/api/additional_documents/additionaldoc', formData2, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      });
      console.log('Additional documents saved successfully');
    } catch (error) {
      console.error('Error saving additional documents:', error);
    }
  };

  return (
    <SoftBox mt={5}>
      <Card sx={{ padding: "35px" }}>
        <SoftTypography
          variant="h6"
          mb={2}
          sx={{ marginLeft: "3px", marginTop: "30px", marginBottom: "30px" }}
        >
          Additional Documents
        </SoftTypography>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <FormField
              label="Additional Document 1"
              placeholder="Enter Additional Document 1 Name"
              value={document1.name}
              onChange={(e) => setDocument1({ ...document1, name: e.target.value })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox lineHeight={0} display="inline-block">
                <SoftTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Choose Additional Document 1
                </SoftTypography>
              </SoftBox>
              <FormField type="file" onChange={(e) => handleFileChange(e, setDocument1)} />
            </SoftBox>
          </Grid>

          <Grid item xs={12} sm={6}>
            <FormField
              label="Additional Document 2"
              placeholder="Enter Additional Document 2 Name"
              value={document2.name}
              onChange={(e) => setDocument2({ ...document2, name: e.target.value })}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <SoftBox display="flex" flexDirection="column" justifyContent="flex-end" height="100%">
              <SoftBox lineHeight={0} display="inline-block">
                <SoftTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Choose Additional Document 2
                </SoftTypography>
              </SoftBox>
              <FormField type="file" onChange={(e) => handleFileChange(e, setDocument2)} />
            </SoftBox>
          </Grid>
        </Grid>
        <SoftButton
          variant="gradient"
          color="info"
          sx={{ marginRight: "15px", width: "100px", marginTop: "40px" }}
          onClick={handleSubmit}
        >
          Save
        </SoftButton>
      </Card>
    </SoftBox>
  );
}

export default AdditionalDocuments;
