
import { createContext, useContext, useReducer, useMemo } from "react";

// prop-types is a library for typechecking of props
import PropTypes from "prop-types";


// The Soft UI Dashboard PROmain context
const SoftUI = createContext(null);
const profileUI = createContext(null);
const agencyUI = createContext(null);



// Setting custom name for the context which is visible on react dev tools
SoftUI.displayName = "SoftUIContext";
profileUI.displayName = "ProfileUIContext";
agencyUI.displayName = "agencyUIContext";


// Soft UI Dashboard PROreducer
function reducer(state, action) {
  switch (action.type) {
    case "MINI_SIDENAV": {
      return { ...state, miniSidenav: action.value };
    }
    case "TRANSPARENT_SIDENAV": {
      return { ...state, transparentSidenav: action.value };
    }
    case "SIDENAV_COLOR": {
      return { ...state, sidenavColor: action.value };
    }
    case "TRANSPARENT_NAVBAR": {
      return { ...state, transparentNavbar: action.value };
    }
    case "FIXED_NAVBAR": {
      return { ...state, fixedNavbar: action.value };
    }
    case "OPEN_CONFIGURATOR": {
      return { ...state, openConfigurator: action.value };
    }
    case "DIRECTION": {
      return { ...state, direction: action.value };
    }
    case "LAYOUT": {
      return { ...state, layout: action.value };
    }
    case "SETPROFILE": {
      return { ...state,  ...action.value };
    }
    case "UPDATEPROFILE": {
      return { ...state,  ...action.value };
    }
    case "SETAGENCY": {
      return { ...state,  ...action.value };
    }
    case "UPDATEAGENCY": {
      return { ...state,  ...action.value };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

// Soft UI Dashboard PROcontext provider
function SoftUIControllerProvider({ children }) {
  const initialState = {
    miniSidenav: false,
    transparentSidenav: true,
    sidenavColor: "info",
    transparentNavbar: true,
    fixedNavbar: true,
    openConfigurator: false,
    direction: "ltr",
    layout: "dashboard",
  };



  const [controller, dispatch] = useReducer(reducer, initialState);

  const value = useMemo(() => [controller, dispatch], [controller, dispatch]);

  return <SoftUI.Provider value={value}>{children}</SoftUI.Provider>;
}

// Soft UI Dashboard PROcustom hook for using context
function useSoftUIController() {
  const context = useContext(SoftUI);

  if (!context) {
    throw new Error("useSoftUIController should be used inside the SoftUIControllerProvider.");
  }

  return context;
}

// Typechecking props for the SoftUIControllerProvider
SoftUIControllerProvider.propTypes = {
  children: PropTypes.node.isRequired,
};



//PRofileUI
function ProfileUIControllerProvider({ children }) {
  const initialState = {
    firstname: "",
    contact_number: "",
    alt_contact_number: "",
    middleName:"",
    lastName:"",
    email: "",
    gender: "",
    linkDate: "",
    confirmationEmail: "",
    techskills:"",
    skills: [],
    education: [],
    hobbies: [],
    indian_states: [],
    linkedin_ids: [],
    linkedin_urls: [],
    country: "",
    state: "",
    city: "",
    noticeperiod: "",
    visastatus: "",
    visatype:"",
    jobtitle:"",
    location: "",
    workinformation: [],
    educationhistory: [],
    referancedetails: [],
    socialdetails: {}
    };

  

  const [controller, dispatch] = useReducer(reducer, initialState);

  const value = useMemo(() => [controller, dispatch], [controller, dispatch]);

  return <profileUI.Provider value={value}>{children}</profileUI.Provider>;
}

function useProfileUIController() {
  const context = useContext(profileUI);

  if (!context) {
    throw new Error("useProfileUIController should be used inside the ProfileUIControllerProvider.");
  }

  return context;
}

// Typechecking props for the SoftUIControllerProvider
ProfileUIControllerProvider.propTypes = {
  children: PropTypes.node.isRequired,
};



//new context


function AgencyUIControllerProvider({ children }) {
  const initialState = {
    shortDescription: "",
    companyName: "",
    otherNames: "",
    networkID: "",
    website: "",
    pin: "",
    address: "",
    city: "",
    country: "",
    state: "",
    workinformation:[],
    };

  

  const [controller, dispatch] = useReducer(reducer, initialState);

  const value = useMemo(() => [controller, dispatch], [controller, dispatch]);

  return <agencyUI.Provider value={value}>{children}</agencyUI.Provider>;
}

function useAgencyUIController() {
  const context = useContext(agencyUI);

  if (!context) {
    throw new Error("useAgencyUIController should be used inside the AgencyUIControllerProvider.");
  }

  return context;
}

// Typechecking props for the SoftUIControllerProvider
AgencyUIControllerProvider.propTypes = {
  children: PropTypes.node.isRequired,
};






// Context module functions
const setMiniSidenav = (dispatch, value) => dispatch({ type: "MINI_SIDENAV", value });
const setTransparentSidenav = (dispatch, value) => dispatch({ type: "TRANSPARENT_SIDENAV", value });
const setSidenavColor = (dispatch, value) => dispatch({ type: "SIDENAV_COLOR", value });
const setTransparentNavbar = (dispatch, value) => dispatch({ type: "TRANSPARENT_NAVBAR", value });
const setFixedNavbar = (dispatch, value) => dispatch({ type: "FIXED_NAVBAR", value });
const setOpenConfigurator = (dispatch, value) => dispatch({ type: "OPEN_CONFIGURATOR", value });
const setDirection = (dispatch, value) => dispatch({ type: "DIRECTION", value });
const setLayout = (dispatch, value) => dispatch({ type: "LAYOUT", value });
const setProfile = (dispatch, value) => dispatch({ type: "SETPROFILE", value });
const setUpdateProfile = (dispatch, value) => dispatch({ type: "UPDATEPROFILE", value });
const setAgency = (dispatch, value) => dispatch({ type: "SETAGENCY", value });
const setUpdateAgency = (dispatch, value) => dispatch({ type: "UPDATEAGENCY", value });


export {
  SoftUIControllerProvider,
  useSoftUIController,
  ProfileUIControllerProvider,
  AgencyUIControllerProvider, // deprecated
  useProfileUIController, //
  setMiniSidenav,
  setTransparentSidenav,
  setSidenavColor,
  setTransparentNavbar,
  setFixedNavbar,
  setOpenConfigurator,
  setDirection,
  setLayout,
  setProfile,
  setUpdateProfile,
  setUpdateAgency,
  setAgency,
  useAgencyUIController
};
